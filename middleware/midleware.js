require("dotenv").config();
const jwt = require("jsonwebtoken");
const { NotAuthenticated, Forbidden } = require("../utils/response.js");

class Authentication {
  async requiredToken(req, res, next) {
    try {
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];

      const payload = jwt.verify(token, process.env.TOKEN_SECRET);
      req.user = payload;
      next();
    } catch (err) {
      res.status(401).send(new NotAuthenticated());
    }
  }

  async mine(req, res, next) {
    try {

      req.user = payload;
      next();
    } catch (err) {
      res.status(401).send(new NotAuthenticated());
    }
  }
}

module.exports = new Authentication();
