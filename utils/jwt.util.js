const jwt = require("jsonwebtoken");

exports.generateToken = async (payload) => {
    return await jwt.sign(payload, process.env.TOKEN_SECRET, {
        expiresIn: "60000s",
    });
};

// exports.decodeToken = async (req) => {
//     const token = req.headers.authorization.substring(
//         7, req.headers.authorization.length);
//     const decodedToken = await jwt.decode(token);

//     return decodedToken;
// };

exports.decodeToken = async (token) => {
    const decodedToken = await jwt.decode(token);

    return decodedToken;
};
