const multer = require("multer");
const path = require("path");
const { NotFound, InternalServerError } = require("./response");
// Multer config
module.exports = multer({
  storage: multer.diskStorage({}),
  fileFilter: (req, file, cb) => {

    let ext = path.extname(file.originalname);
    if (ext !== ".jpg" && ext !== ".jpeg" && ext !== ".png") {
      cb(new InternalServerError(), false);
      return;
    }
    cb(null, true);

  },
});
