const express = require("express");

const NotifService = require("../../../services/notif.service.js");
const userService = require("../../../services/user.service.js");
const { SuccessFetchResponse } = require("../../../utils/response.js");

class NotifController {
  async getnotifs(req, res) {
    // try {
      const data = await NotifService.getAllNotif(req);
      return SuccessFetchResponse(res, data);
    // } catch (err) {
    //   res.status(err.status).send(err);
    // }
  }
  async getNotifById(req, res) {
    const id = req.params.id;
    try {
      const data = await NotifService.getOneById(id);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }
  async addNotif(req, res) {
    try {
      const data = await NotifService.createNotif(req, req.body, req.user);

      console.log(req.body);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }
  async updateNotif(req, res) {
    try {
      const id = req.params.id;
      const data = await NotifService.updateNotif(id, req, req.body, req.user);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }
  async deleteNotif(req, res) {
    try {
      const id = req.params.id;
      const data = await NotifService.deleteNotif(id, req.user);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }
}

module.exports = new NotifController();


// function notivication(io){
  
//   const router = express.Router();

//   router.get("/notif", (req, res) => {
    
//     const id = req.query.id;
//     if (!id){
//       res
//         .json({
//           message: "id not exist",
//         })
//         .status(401);
//     }
//       io.emit("notification", id);
//       res.json({
//         message: `data delivered : ${id}`,
//       })
//   });

//   return router;
// }

// module.exports = notivication;


// class NotifController {
//   success(req, res) {
//     res.status(200).json({
//         status: "OK",
//         message: "SH API running!",
//     });
//   }
//   async getNotif(_, res) {
//     try {
//       const data = await NotifService.getAllNotif();
//       return SuccessFetchResponse(res, data);
//     } catch (err) {
//       // res.status(err.status).send(err);
//     }
//   }
//   async getNotifById(req, res) {
//     const id = req.params.id;
//     try {
//       const data = await NotifService.getOneById(id);
//       return SuccessFetchResponse(res, data);
//     } catch (err) {
//       res.status(err.status).send(err);
//     }
//   }
//   async addNotif(req, res) {
//     try {
//       const user = await userService.currentUser(req);
//       const data = await NotifService.createNotif(req.body, user);

//       console.log(req.body);
//       return SuccessFetchResponse(res, data);
//     } catch (err) {
//       res.status(err.status).send(err);
//     }
//   }
//   async updateNotif(req, res) {
//     try {
//       const id = req.params.id;
//       const user = await userService.currentUser(req);
//       const data = await NotifService.updateNotif(id, req.body, user);
//       return SuccessFetchResponse(res, data);
//     } catch (err) {
//       res.status(err.status).send(err);
//     }
//   }
//   async deleteNotif(req, res) {
//     // try {
//     const id = req.params.id;
//     const data = await NotifService.deleteNotif(id, req.user);
//     return SuccessFetchResponse(res, data);
//     // } catch (err) {
//     //   res.status(err.status).send(err);
//     // }
//   }
// }

// module.exports = new NotifController();
