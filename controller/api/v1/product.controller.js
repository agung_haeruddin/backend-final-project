const ProductService = require("../../../services/product.service.js");
const { SuccessFetchResponse } = require("../../../utils/response.js");
const userService = require("../../../services/user.service.js");
const orderService = require("../../../services/order.service.js");

class ProductController {
  getRoot(req, res) {
    try {
      res.status(200).json({
        status: "OK",
        message: "SH API running!",
      });
    } catch (err) {
      res.status(err.status).send(err);
    }
  }

  async getCategoryProducts(req, res) {
    try {
      const data = await ProductService.categoryProducts(req);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }

  async searchProducts(req, res) {
    try {
      const data = await ProductService.searchProducts(req);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }
  async getProducts(req, res) {
    try {
      const data = await ProductService.getAllProducts(req, req.user,);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }

  async getProductByUser(req, res) {
    try {
      const data = await ProductService.getAllProductsByUser(req, req.user,);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }




  async getProductById(req, res) {

    try {
      const id = req.params.id;
      const data = await ProductService.getOneById(id);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }
  async addProduct(req, res) {
    try {
      const data = await ProductService.createProduct(
        req,
        req.body,
        req.user,
        req.param
      );
      console.log(req.body);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }
  async updateProduct(req, res) {
    try {
      const id = req.params.id;
      const status = req.query.status;
      const data = await ProductService.updateProduct(
        id,
        req.body,
        req.user,
        req,
        status
      );
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }
  async deleteProduct(req, res) {
    try {
      const id = req.params.id;
      const data = await ProductService.deleteProduct(id, req.user);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }
}

module.exports = new ProductController();
