const OrderService = require("../../../services/order.service.js");
const userService = require("../../../services/user.service.js");
const { SuccessFetchResponse } = require("../../../utils/response.js");

class OrderController {
  async getOrders(req, res) {
    try {
      const data = await OrderService.getAllOrders(req);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }

  async getOrdersWish(req, res) {

    try {

      const data = await OrderService.getWishlist(req.user);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }

  async getOrderSold(req, res) {
    try {
      console.log(req.user)
      const data = await OrderService.getSold(req.user);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }
  async getOrderById(req, res) {
    const id = req.params.id;
    try {
      const data = await OrderService.getOneById(id);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }

  async getOrderByStatusId(req, res) {
    const id = req.params.id;
    try {
      const data = await OrderService.getOneByStatusId(id, req.user);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }

  async getWishlistById(req, res) {

    try {
      const data = await OrderService.getOneWishlistById(req.params.id, req.params.status, req.user);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }

  async addOrder(req, res) {
    try {
      const data = await OrderService.createOrder(req, req.body, req.user);

      res.status(201).json({ message: "successfully" });
    } catch (err) {
      res.status(err.status).send(err);
    }
  }
  async updateOrder(req, res) {
    // try {
    const id = req.params.id;
    const data = await OrderService.updateOrder(id, req, req.user);
    res.status(200).json({ message: "successfully" });
    // } catch (err) {
    //   res.status(err.status).send(err);
    // }
  }
  async deleteOrder(req, res) {
    try {
      const id = req.params.id;
      const data = await OrderService.deleteOrder(id, req.user);
      return SuccessFetchResponse(res, data);
    } catch (err) {
      res.status(err.status).send(err);
    }
  }
}

module.exports = new OrderController();
