const db = require("../../../models/index.js");
const User = db.user;
const userService = require("../../../services/user.service.js");
const jwtUtil = require("../../../utils/jwt.util.js");
const bcrypt = require("bcrypt");

exports.createNewUserApi = async (req, res) => {
  try {
    const user = await userService.createUser(req);

    res.status(201).json({ message: "Updated successfully" });
  } catch (err) {
    res.status(err.status).send(err);
  }
};

exports.loginUserApi = async (req, res) => {
  try {
    const user = await userService.loginUser(req);

    if (user) {
      const payloadToken = {
        id: user.id,
        user_name: user.user_name,
        email: user.email,
      };

      const tokens = await jwtUtil.generateToken(payloadToken);
      console.log(tokens);

      res.status(200).json({ token: tokens });
      console.info(tokens);
    } else {
      res.status(401).json({ error: "Wrong login credetntial" });
    }
  } catch (err) {
    res.status(err.status).send(err);
  }
};

exports.userProfileApi = async (req, res) => {
  try {
    const user = await userService.currentUser(req);

    res.json({
      user_name: user.user_name,
      phone: user.phone,
      photo: user.photo,
      address: user.address,
    });
  } catch (err) {
    res.status(err.status).send(err);
  }
};


exports.updateUserApi = async (req, res) => {
  try {
    const token = req.headers.authorization.substring(
      7,
      req.headers.authorization.length
    );
    const decodedToken = await jwtUtil.decodeToken(token);
    const userId = await User.findByPk(decodedToken.id);

    console.log(`User id detected is`, userId.id);

    const user = await userService.updateUser(req, userId.id);

    if (user == null) {
      res.status(404).json({
        error: `User not found with Id : ${userId.id}`,
      });
    } else {
      res.json({ message: "Updated successfully" });
    }
  } catch (err) {
    res.status(err.status).send(err);
  }
};
