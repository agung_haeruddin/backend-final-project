const models = require("../models/index");
const { Op, or } = require("sequelize");
const { NotFound, InternalServerError } = require("../utils/response");
const { json, raw } = require("body-parser");

class OrderService {
  async getAllOrders(req, res) {
    try {
      const pageAsNumber = Number.parseInt(req.query.page);
      const sizeAsNumber = Number.parseInt(req.query.size);
      let page = 0;
      if (!Number.isNaN(pageAsNumber) && pageAsNumber > 1) {
        page = pageAsNumber;
      }

      let size = 10;
      if (
        !Number.isNaN(sizeAsNumber) &&
        sizeAsNumber > 0 &&
        sizeAsNumber < 10
      ) {
        size = sizeAsNumber;
      }
      const cars = await models.orders.findAndCountAll({
        limit: size,
        offset: page * page,
        include: [
          {
            model: models.status,
            attributes: ["id", "status_name"],
          },
          {
            model: models.user,
            attributes: ["id", "photo", "user_name", "address"],
          },
          {
            model: models.product,
            include: [
              {
                model: models.user,
                attributes: ["id", "photo", "user_name", "address"],
              },
              {
                model: models.category,
                attributes: ["id", "category_name"],
              },
            ],
            attributes: {
              exclude: ["createdAt", "updatedAt"],
            },
          },
        ],
      });
      return { content: cars.rows, totalPages: Math.ceil(cars.count / size) };
    } catch (err) {
      throw new InternalServerError();
    }
  }
  async getOneById(id) {
    try {
      const product = await models.orders.findOne({
        where: {
          id: id,
        },
        include: [
          {
            model: models.status,
            attributes: ["id", "status_name"],
          },
          {
            model: models.user,
            attributes: ["id", "photo", "user_name", "address"],
          },
          {
            model: models.product,
            include: [
              {
                model: models.user,
                attributes: ["id", "photo", "user_name", "address"],
              },
              {
                model: models.category,
                attributes: ["id", "category_name"],
              },
            ],
            attributes: {
              exclude: ["createdAt", "updatedAt"],
            },
          },
        ],
      });
      return product;
    } catch (err) {
      throw new InternalServerError();
    }
  }
  async getOneByStatusId(id, user) {
    try {
      const product = await models.product.findAll({
        where: {
          user_id: user?.id,
        },
        raw: true,
        nest: true,
      })
      if (!product) {
        throw new NotFound
      }
      const orders = await models.orders.findAll({
        where: {
          [Op.or]: [{
            status_id: id
          }],
        },
        raw: true,
      })

      const data = product.map((row) => row.id)
      const data2 = orders.map((row) => row.product_id)

      const filtered = data.filter(r => data2.find(obj => obj == r) > -1)

      const order = await models.orders.findAll({
        where: {
          [Op.or]: [{ product_id: filtered.map(raw => raw) }],
          status_id: id

        },
        raw: true,
        nest: true,
        include: [
          {
            model: models.status,
            attributes: ["id", "status_name"],
          },
          {
            model: models.user,
            attributes: ["id", "photo", "user_name", "address"],
          },
          {
            model: models.product,
            include: [
              {
                model: models.user,
                attributes: ["id", "photo", "user_name", "address"],
              },
              {
                model: models.category,
                attributes: ["id", "category_name"],
              },
            ],
            attributes: {
              exclude: ["createdAt", "updatedAt"],
            },
          },
        ],
      })

      console.log(order)
      console.log(data, data2, filtered)
      return order

    } catch (err) {
      throw new InternalServerError();
    }
  }

  async getOneWishlistById(id, status) {
    try {
      const order = await models.orders.findOne({
        where: {
          id: id,
          status_id: status,
        },

        include: [
          {
            model: models.status,
            attributes: ["id", "status_name"],
          },
          {
            model: models.user,
            attributes: ["id", "photo", "user_name", "address", "phone"],
          },
          {
            model: models.product,
            include: [
              {
                model: models.user,
                attributes: ["id", "photo", "user_name", "address", "phone"],
              },
              {
                model: models.category,
                attributes: ["id", "category_name"],
              },
            ],

          },
        ],
      });

      return order

    } catch (err) {
      throw new InternalServerError();
    }
  }
  async getWishlist(user) {
    try {
      const product = await models.product.findAll({
        where: {
          user_id: user?.id,
        },
        raw: true,
        nest: true,
      })
      if (!product) {
        throw new NotFound
      }
      const orders = await models.orders.findAll({
        where: {
          [Op.or]: [
            { status_id: 2 },
            { status_id: 1 },

          ]
        },
        raw: true,
      });

      const data = product.map((row) => row.id)
      const data2 = orders.map((row) => row.product_id)

      const filtered = data.filter(r => data2.find(obj => obj == r) > -1)

      const order = await models.orders.findAll({
        where: {
          [Op.or]: [{ product_id: filtered.map(raw => raw) },
          ],
          status_id: {
            [Op.not]: [3, 4]
          }

        },
        raw: true,
        nest: true,
        include: [
          {
            model: models.status,
            attributes: ["id", "status_name"],
          },
          {
            model: models.user,
            attributes: ["id", "photo", "user_name", "address", "phone"],
          },
          {
            model: models.product,
            include: [
              {
                model: models.user,
                attributes: ["id", "photo", "user_name", "address", "phone"],
              },
              {
                model: models.category,
                attributes: ["id", "category_name"],
              },
            ],
            attributes: {
              exclude: ["createdAt", "updatedAt"],
            },
          },
        ],
      })


      return order
    } catch (err) {
      throw new InternalServerError();
    }
  }

  async getSold(user) {
    try {
      const product = await models.orders.findAll({
        where: {
          user_id: user?.id,
          status_id: 4,
        },
        include: [
          {
            model: models.status,
            attributes: ["id", "status_name"],
          },
          {
            model: models.user,
            attributes: ["id", "photo", "user_name", "address"],
          },
          {
            model: models.product,
            include: [
              {
                model: models.user,
                attributes: ["id", "photo", "user_name", "address", "phone"],
              },
              {
                model: models.category,
                attributes: ["id", "category_name"],
              },
            ],
            attributes: {
              exclude: ["createdAt", "updatedAt"],
            },
          },
        ],
      });
      return product;
    } catch (err) {
      throw new InternalServerError();
    }
  }

  async createOrder(req, data, user) {
    try {
      const { status_id, product_id } = req.query;
      const user_id = user?.id;
      const bid = req.body.bid;

      const orders = await models.orders.create({
        product_id,
        user_id,
        status_id,
        bid,
      });

      return orders;
    } catch (err) {
      throw new InternalServerError();
    }
  }

  async updateOrder(id, req, user) {
    // try {
    const isExists = await models.orders.findOne({
      where: {
        id: id,
      },
      raw: true,
      nest: true,
      include: [
        {
          model: models.product,
          include: [
            {
              model: models.user,
              attributes: ["id", "photo", "user_name", "address", "phone"],
            },
          ]
        }
      ]

    });

    if (!isExists) {
      throw new NotFound();
    }

    // const isUser = await models.users.findOne({
    //   where: {
    //     id: user?.id,
    //   },
    // });
    // if (!isUser) {
    //   throw new NotFound();
    // }

    const { status_id } = req.query;
    const order = await models.orders.update(
      {
        status_id,
      },
      { where: { id } }
    );

    return order;
    //   } catch (err) {
    //     throw new InternalServerError();
    //   }
  }
  async deleteOrder(id, user) {
    try {
      const isExists = await models.orders.findOne({
        where: {
          id: id,
        },
      });

      if (!isExists) {
        throw new NotFound();
      }

      models.orders.destroy({
        where: {
          id: id,
        },
      });

      return `${id} berhasil di hpus`;
    } catch (err) {
      throw new InternalServerError();
    }
  }
}

module.exports = new OrderService();
