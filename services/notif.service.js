const models = require("../models/index");

const { NotFound, InternalServerError } = require("../utils/response");

class NotifService {
  async getAllNotif(req, res) {
    // try {
      const pageAsNumber = Number.parseInt(req.query.page);
      const sizeAsNumber = Number.parseInt(req.query.size);
      let page = 0;
      if (!Number.isNaN(pageAsNumber) && pageAsNumber > 1) {
        page = pageAsNumber;
      }

      let size = 10;
      if (
        !Number.isNaN(sizeAsNumber) &&
        sizeAsNumber > 0 &&
        sizeAsNumber < 10
      ) {
        size = sizeAsNumber;
      }
      const cars = await models.notif.findAndCountAll({
        limit: size,
        offset: page * page,
        include: [
          {
            model: models.user,
            attributes: ["id", "photo", "user_name", "address"],
          },
          {
            model: models.orders,
            attributes: ["id", "bid"],
          },
          {
            model: models.product,
            include: [
              {
                model: models.user,
                attributes: ["id", "photo", "user_name", "address"],
              },
              {
                model: models.category,
                attributes: ["id", "category_name"],
              },
            ],
            attributes: {
              exclude: ["createdAt", "updatedAt"],
            },
          },
        ],
      });
      return { content: cars.rows, totalPages: Math.ceil(cars.count / size) };
    // } catch (err) {
    //   throw new InternalServerError();
    // }
  }
  async getOneById(id) {
    const product = await models.notif.findOne({
      where: {
        id: id,
      },
      include: [
        {
          model: models.user,
          attributes: ["id", "photo", "user_name", "address"],
        },
        {
          model: models.product,
          include: [
            {
              model: models.user,
              attributes: ["id", "photo", "user_name", "address"],
            },
            {
              model: models.category,
              attributes: ["id", "category_name"],
            },
          ],
          attributes: {
            exclude: ["createdAt", "updatedAt"],
          },
        },
        {
          model: models.orders,
          attributes: ["id", "bid"],
        },
      ],
    });
    return product;
  }

  async createNotif(req, data, user) {
    try {
      const { product_id, order_id } = req.query;
      const user_id = user.id;

      console.log(bid);

      const Notif = await models.notif.create({
        user_id,
        product_id,
        order_id
      });

      return Notif;
    } catch (error) {
      throw new InternalServerError();
    }
  }

  async updateNotif(id, req, data, user) {
    try {
      const isExists = await models.notif.findOne({
        where: {
          id: id,
        },
      });
      if (!isExists) {
        throw new NotFound();
      }
      const { product_id, order_id } = req.query;

      const user_id = user.id;
      const Notif = await models.notif.update(
        {
          user_id,
          product_id,
          order_id,
        },
        { where: { id } }
      );

      if (Notif) return `data ${id} berhasil di update`;
    } catch (error) {
      throw new InternalServerError();
    }
  }
  async deleteNotif(id, user) {
    try {
      const isExists = await models.notif.findOne({
        where: {
          id: id,
        },
      });

      if (!isExists) {
        throw new NotFound();
      }

      models.notif.destroy({
        where: {
          id: id,
        },
      });

      return `${id} berhasil di hpus`;
    } catch (error) {
      throw new InternalServerError();
    }
  }
}

module.exports = new NotifService();
