const models = require("../models/index");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const cloudinary = require("../utils/cloudinary");
const Upload = require("../utils/upload");

const { NotFound, InternalServerError } = require("../utils/response");

class ProductService {
  async searchProducts(req, res) {
    try {
      const pageAsNumber = Number.parseInt(req.query.page);
      const sizeAsNumber = Number.parseInt(req.query.size);
      const { term } = req.query;
      let page = 0;
      if (!Number.isNaN(pageAsNumber) && pageAsNumber > 1) {
        page = pageAsNumber;
      }

      let size = 10;
      if (
        !Number.isNaN(sizeAsNumber) &&
        sizeAsNumber > 0 &&
        sizeAsNumber < 10
      ) {
        size = sizeAsNumber;
      }
      const product = await models.product.findAndCountAll({
        where: {
          [Op.or]: [
            { product_name: { [Op.like]: "%" + term + "%" } },

            { product_description: { [Op.like]: "%" + term + "%" } },
          ],
        },

        limit: size,
        offset: page * page,
        include: [
          {
            model: models.user,
          },
          {
            model: models.category,
          },
        ],
      });
      return {
        content: product.rows,
        totalPages: Math.ceil(product.count / size),
      };
    } catch (err) {
      throw new InternalServerError();
    }
  }

  async categoryProducts(req, res) {
    try {
      const pageAsNumber = Number.parseInt(req.query.page);
      const sizeAsNumber = Number.parseInt(req.query.size);
      const term = req.query.category;
      const obj = JSON.parse(term)
      let page = 0;
      if (!Number.isNaN(pageAsNumber) && pageAsNumber > 1) {
        page = pageAsNumber;
      }

      let size = 10;
      if (
        !Number.isNaN(sizeAsNumber) &&
        sizeAsNumber > 0 &&
        sizeAsNumber < 10
      ) {
        size = sizeAsNumber;
      }

      const product = await models.product.findAndCountAll({
        where: {
          [Op.or]: [{
            category_id: obj.map(row => row)
          }],
        },

        limit: size,
        offset: page * page,
        include: [
          {
            model: models.user,
          },
          {
            model: models.category,
          },
        ],
      });
      return {
        content: product.rows,
        totalPages: Math.ceil(product.count / size),
      };
    } catch (err) {
      throw new InternalServerError();
    }
  }

  async getAllProducts(req, user) {
    try {
      const pageAsNumber = Number.parseInt(req.query.page);
      const sizeAsNumber = Number.parseInt(req.query.size);

      let page = 0;
      if (!Number.isNaN(pageAsNumber) && pageAsNumber > 1) {
        page = pageAsNumber;
      }

      let size = 10;
      if (
        !Number.isNaN(sizeAsNumber) &&
        sizeAsNumber > 0 &&
        sizeAsNumber < 10
      ) {
        size = sizeAsNumber;
      }
      const product = await models.product.findAndCountAll({
        limit: size,
        offset: page * page,
        include: [
          {
            model: models.user,
            attributes: ["id", "photo", "user_name", "address"],
          },
          {
            model: models.category,
            attributes: ["id", "category_name"],
          },
        ],
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      });
      return {
        content: product.rows,
        totalPages: Math.ceil(product.count / size),
      };
    } catch (err) {
      throw new InternalServerError();
    }
  }

  async getAllProductsByUser(req, user) {
    try {
      const pageAsNumber = Number.parseInt(req.query.page);
      const sizeAsNumber = Number.parseInt(req.query.size);

      let page = 0;
      if (!Number.isNaN(pageAsNumber) && pageAsNumber > 1) {
        page = pageAsNumber;
      }

      let size = 10;
      if (
        !Number.isNaN(sizeAsNumber) &&
        sizeAsNumber > 0 &&
        sizeAsNumber < 10
      ) {
        size = sizeAsNumber;
      }
      const product = await models.product.findAndCountAll({
        where: {
          user_id: user?.id
        },
        limit: size,
        offset: page * page,
        include: [
          {
            model: models.user,
            attributes: ["id", "photo", "user_name", "address"],
          },
          {
            model: models.category,
            attributes: ["id", "category_name"],
          },
        ],
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      });
      return {
        content: product.rows,
        totalPages: Math.ceil(product.count / size),
      };
    } catch (err) {
      throw new InternalServerError();
    }
  }



  async getOneById(id) {
    try {
      const product = await models.product.findOne({
        where: {
          id: id,
        },
        include: [
          {
            model: models.user,
            attributes: ["id", "photo", "user_name", "address"],
          },
          {
            model: models.category,
            attributes: ["id", "category_name"],
          },
        ],
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      });
      return product;
    } catch (err) {
      throw new InternalServerError();
    }
  }

  async createProduct(req, data, user) {
    try {
      const urls = [];
      const files = req.files;
      for (const file of files) {
        const { path } = file;
        const newPath = await Upload.cloudinaryImageUploadMethod(path);
        urls.push(newPath);
      }

      const { product_name, product_description, product_price, category_id } =
        data;
      // const product_name = req.fields.product_name;
      // const product_description = req.fields.product_description;
      // const product_price = req.fields.product_price;
      // const category_id = req.fields.product_category_id;

      const status = req.query.status;

      const user_id = user.id;
      console.log(user_id);

      const product_image = urls.map((url) => url.res);
      const cloudinary_id = urls.map((url) => url.id);

      console.log(cloudinary_id);

      const product = await models.product.create({
        product_name,
        product_image,
        product_description,
        product_price,
        user_id,
        category_id,
        cloudinary_id,
        status,
      });
      return product;
    } catch (err) {
      throw new InternalServerError();
    }
  }
  async updateProduct(id, data, user, req) {
    try {
      const isExists = await models.product.findOne({
        where: {
          id: id,
        },
      });
      if (!isExists) {
        throw new NotFound();
      }

      const urls = [];
      const files = req.files;
      for (const file of files) {
        const { path } = file;
        const newPath = await Upload.cloudinaryImageUploadMethod(path);
        urls.push(newPath);
      }
      const { product_description, product_price, category_id, product_name } =
        data;

      const user_id = user.id;
      const status = req.query.status;
      const product_image = urls.map((url) => url.res);
      const cloudinary_id = urls.map((url) => url.id);

      const product = await models.product.update(
        {
          product_name,
          product_image,
          product_description,
          product_price,
          user_id,
          category_id,
          cloudinary_id,
          status,
        },
        { where: { id } }
      );
      if (product) return `data ${id} berhasil di update`;
    } catch (error) {
      throw new InternalServerError();
    }
  }
  async deleteProduct(id, user) {
    try {
      const isExists = await models.product.findOne({
        where: {
          id: id,
        },
      });

      const myArray = JSON.parse(isExists.cloudinary_id);

      for (const file of myArray) {
        await cloudinary.uploader.destroy(file);
      }
      console.log(myArray);

      if (!isExists) {
        throw new NotFound();
      }

      models.product.destroy({
        where: {
          id: id,
        },
      });

      return `${id} berhasil di hpus`;
    } catch (error) {
      throw new InternalServerError();
    }
  }
}

module.exports = new ProductService();
