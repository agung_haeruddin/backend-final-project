const db = require("../models/index.js");
const models = require("../models/index");
const User = db.user;

const bcrypt = require("bcrypt");
const jwtUtil = require("../utils/jwt.util.js");
const cloudinaryConfig = require("../config/cloudinary.config.js");
const { NotFound, InternalServerError } = require("../utils/response");

// Menggunakan body parser
// exports.createUser = async (req) => {
//     const salt = await bcrypt.genSalt(10);
//     const encryptedPassword = await bcrypt.hash(req.body.password, salt);

//     const user = {
//         user_name: req.body.user_name,
//         email: req.body.email,
//         password: encryptedPassword,
//     };
//     console.log(user);

//     if (user != null) {
//         const buat = await User.create(user);

//         return buat;
//     } else {
//         return null;
//     }
// };

// exports.loginUser = async (req) => {
//     const user = await await User.findOne({ where: { email: req.body.email } });

//     if (user != null) {
//         const checkPassword = await bcrypt.compare(
//             req.body.password,
//             user.password
//         );

//         if (checkPassword) {
//             return user;
//         } else {
//             return null;
//         }
//     } else {
//         return null;
//     }
// };

// exports.currentUser = async (req) => {
//     const token = req.headers.authorization.substring(
//         7,
//         req.headers.authorization.length
//     );
//     const decodedToken = await jwtUtil.decodeToken(token);
//     const user = await User.findByPk(decodedToken.id);

//     console.log(`User id detected is`, decodedToken.id);

//     return user;
// };

// exports.updateUser = async (req, id) => {
//     try {
//         // const uploadProfile = await cloudinaryConfig.uploader.upload(
//         //     req.files.photo.path
//         // );

//         const user = {
//             user_name: req.body.user_name,
//             address: req.body.address,
//             phone: req.body.phone,
//             // photo: uploadProfile.secure_url,
//         };
//         console.log(user);

//         const userById = await User.findByPk(id);

//         if (userById == null) {
//             return null;
//         } else {
//             return await User.update(user, { where: { id: id } });
//         }
//     } catch (err) {
//         console.error(err);
//     }
// };

//
//
//

exports.createUser = async (payload) => {
  try {
    const salt = await bcrypt.genSalt(10);
    const encryptedPassword = await bcrypt.hash(payload.body.password, salt);
    console.log(payload.body.user_name);
    const user = {
      user_name: payload.body.user_name,
      email: payload.body.email,
      password: encryptedPassword,
    };

    if (user != null) {
      const buat = await User.create(user);
      console.log(buat);
      return buat;
    } else {
      return null;
    }

  } catch (err) {
    throw new InternalServerError();
  }
};

exports.loginUser = async (payload) => {
  try {

    const user = await User.findOne({
      where: { email: payload.body.email },
    });

    if (user != null) {
      const checkPassword = await bcrypt.compare(
        payload.body.password,
        user.password
      );

      if (checkPassword) {
        return user;
      } else {
        return null;
      }
    } else {
      return null;
    }
  } catch (err) {
    throw new InternalServerError();
  }
};

exports.currentUser = async (req) => {
  try {
    const token = req.headers.authorization.substring(
      7,
      req.headers.authorization.length
    );
    const decodedToken = await jwtUtil.decodeToken(token);
    const user = await User.findByPk(decodedToken.id);

    console.log(`User id detected is`, decodedToken.id);

    return user;
  } catch (err) {
    throw new InternalServerError();
  }
};

exports.updateUser = async (payload, id) => {
  try {
    const uploadProfile = await cloudinaryConfig.uploader.upload(
      payload.file.path
    );

    const user = {
      user_name: payload.body.user_name,
      address: payload.body.address,
      phone: payload.body.phone,
      photo: uploadProfile.secure_url,
    };

    const userById = await User.findByPk(id);

    if (userById == null) {
      return null;
    } else {
      return await User.update(user, { where: { id: id } });
    }
  } catch (err) {
    throw new InternalServerError();
  }
};
