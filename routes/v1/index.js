const v1 = require("express").Router();
const upload = require("../../utils/multer");
const userController = require("../../controller/api/v1/users.controller");
const ProductController = require("../../controller/api/v1/product.controller.js");
const OrderController = require("../../controller/api/v1/order.controller.js");
const NotifController = require("../../controller/api/v1/notif.controller.js");
const authMiddleware = require("../../middleware/middleware.js");
const Authentication = require("../../middleware/midleware.js");
const {
  notifSeller,
  notification,
} = require("../../controller/api/v1/notificationControllerv2");

// cars
v1.get("/search", ProductController.searchProducts);
v1.get("/", ProductController.getRoot);
v1.get("/products/category", ProductController.getCategoryProducts);
v1.get("/products", ProductController.getProducts);
v1.get("/products/user", Authentication.requiredToken, ProductController.getProductByUser);
v1.get("/product/:id", ProductController.getProductById);
v1.post(
  "/products",
  [Authentication.requiredToken, upload.array("image", 3)],
  ProductController.addProduct
);
v1.put(
  "/product/:id",
  [Authentication.requiredToken, upload.array("image", 3)],
  ProductController.updateProduct
);
v1.delete(
  "/product/:id",
  Authentication.requiredToken,
  ProductController.deleteProduct
);



//user
v1.get("/orders", Authentication.requiredToken, OrderController.getOrders);
v1.get(
  "/order/:id",
  Authentication.requiredToken,
  OrderController.getOrderById
);
v1.get(
  "/order/wishlist/:status/:id",
  Authentication.requiredToken,
  OrderController.getWishlistById
);
v1.get(
  "/order/status/:id",
  Authentication.requiredToken,
  OrderController.getOrderByStatusId
);

v1.get(
  "/orders/wish",
  Authentication.requiredToken,
  OrderController.getOrdersWish
);
v1.get(
  "/orders/sold",
  Authentication.requiredToken,
  OrderController.getOrderSold
);
v1.post("/orders", Authentication.requiredToken, OrderController.addOrder);
v1.put("/order/:id", OrderController.updateOrder);
v1.delete("/order/:id", Authentication.requiredToken, OrderController.deleteOrder);

//Notif
v1.get("/notifs", NotifController.getnotifs);
v1.get("/notif/:id", NotifController.getNotifById);
v1.post("/notif", NotifController.addNotif);
v1.put("/notif/:id", NotifController.updateNotif);
v1.delete("/notif/:id", NotifController.deleteNotif);

// // USER
v1.post("/login", userController.loginUserApi);
v1.post("/register", userController.createNewUserApi);
v1.put("/profile", upload.single("photo"), userController.updateUserApi);
v1.get("/profile", userController.userProfileApi);

// Notifikasi
// v1.get(
//     "/api/v1/user/:userId/:productId/:bargainId/notification/",
//     notification
// );
// v1.get("/api/v1/user/:userId/notification/", notification);
// v1.get("/api/v1/user/:userId/notification/", notifSeller);
// v1.get("/api/v1/user/:userId/notification/", notifBuyer);

module.exports = v1;
