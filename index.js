require("dotenv").config();
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const routes = require("./routes/v1/index");
const ui = require("swagger-ui-express");
const YAML = require("yamljs");
const document = YAML.load("./swagger.yaml");
const formidable = require("express-formidable-v2");
const NotifController = require("./controller/api/v1/notif.controller.js");

const env = process.env;

const app = express();
const server = require('http').createServer(app);
const io = require("socket.io")(server, {
  cors: {
    origin: "*"
  },
});
// const socketRouter = require("./controller/api/v1/notif.controller")(io);

// app.use("/", socketRouter);
// app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// app.use(formidable());

app.use(cors());

app.use(routes);

io.sockets.on("connection", function (socket) {
  console.log("user connect " + socket.id);
  socket.on('send_message', (data) => {
    socket.broadcast.emit("receive_message", data);
    console.log(data);
  })
  // socket.on('send_message', (data) => {
  //   NotifController.getNotifById(data);
  // })
  socket.on('disconnect', function () {
    console.log("Disconnect")
  })
  // socket.on('sendNotification', (data) => {
  //     NotifController.addNotif(data);
  // })
  // socket.on('getNotification', function(){
  //     NotifController.getnotifs();
  // })
  // socket.on('getNotificationById', function(){
  //     NotifController.getNotifById();
  // })
  // socket.on('updateNotification', function(){
  //     NotifController.updateNotif();
  // })
  // socket.on('deleteNotification', function(){
  //     NotifController.deleteNotif();
  // })

})
server.listen(process.env.PORT || port, () => {
  console.info(`App listening at http://localhost:${env.PORT}`);
});

app.use("/doc", ui.serve, ui.setup(document));
app.get("/api-docs", (req, res) => {
  res.sendFile(__dirname + "/swagger.yaml");
});

module.exports = app;

