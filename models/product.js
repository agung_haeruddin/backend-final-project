"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      product.belongsTo(models.user, {
        foreignKey: "user_id",
      });
      product.belongsTo(models.category, {
        foreignKey: "category_id",
      });
    }
  } 
  product.init(
    {
      product_name: DataTypes.STRING,
      product_image: DataTypes.JSON,
      product_description: DataTypes.STRING,
      product_price: DataTypes.INTEGER,
      user_id: DataTypes.INTEGER,
      category_id: DataTypes.INTEGER,
      cloudinary_id: DataTypes.JSON,
      status: DataTypes.INTEGER
    },
    {
      sequelize,
      modelName: "product",
    }
  );
  return product;
};
