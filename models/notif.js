'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class notif extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      notif.belongsTo(models.user, {
        foreignKey: "user_id",
      });
      notif.belongsTo(models.product, {
        foreignKey: "product_id",
      });
      notif.belongsTo(models.orders, {
        foreignKey: "order_id",
      });
    }
  }
  notif.init({
    user_id: DataTypes.INTEGER,
    product_id: DataTypes.INTEGER,
    order_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'notif',
  });
  return notif;
};