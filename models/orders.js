
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class orders extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      orders.belongsTo(models.product, {
        foreignKey: "product_id",
      });
      orders.belongsTo(models.user, {
        foreignKey: "user_id",
      });
      orders.belongsTo(models.status, {
        foreignKey: "status_id",
      });
    }
  }
  orders.init(
    {
      product_id: DataTypes.INTEGER,
      status_id: DataTypes.INTEGER,
      user_id: DataTypes.INTEGER,
      bid: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "orders",
    }
  );
  return orders;
};
