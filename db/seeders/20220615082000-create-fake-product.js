module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "products",
      [
        {
          product_name: "Miniatur mobil klasik Chevrolet Impala",
          product_image:
            '["https://res.cloudinary.com/dkmcpy5fw/image/upload/v1656062679/o7lkcqyhzy4dcjxdnra3.jpg"]',
          product_description: "Nego tipis",
          product_price: 2000000,
          user_id: 1,
          category_id: 1,
          cloudinary_id: '["o7lkcqyhzy4dcjxdnra3"]',
          status: 1,
          createdAt: "2022-06-24T09:24:40.221Z",
          updatedAt: "2022-06-24T09:24:40.221Z",
        },
        {
          product_name: "Joran",
          product_image:
            '["https://res.cloudinary.com/dkmcpy5fw/image/upload/v1656063500/nzjpubi2j42d2wwfeejq.jpg"]',
          product_description: "Joran 1 set",
          product_price: 200000,
          user_id: 1,
          category_id: 1,
          cloudinary_id: '["nzjpubi2j42d2wwfeejq"]',
          status: 1,
          createdAt: "2022-06-24T09:38:20.660Z",
          updatedAt: "2022-06-24T09:38:20.660Z",
        },
        {
          product_name: "Harley DavidChild",
          product_image:
            '["https://res.cloudinary.com/dkmcpy5fw/image/upload/v1656063762/row6slcf7eeihjim8ppa.jpg"]',
          product_description: "Full modif, bisa nego",
          product_price: 20000000,
          user_id: 1,
          category_id: 2,
          cloudinary_id: '["row6slcf7eeihjim8ppa"]',
          status: 1,
          createdAt: "2022-06-24T09:42:42.406Z",
          updatedAt: "2022-06-24T09:42:42.406Z",
        },
        {
          product_name: "Kapal Colombus",
          product_image:
            '["https://res.cloudinary.com/dkmcpy5fw/image/upload/v1656063867/e9b48gike9oqpy0wypwv.jpg"]',
          product_description: "Pemakaian 1 tahun, nego tipis",
          product_price: 200000000,
          user_id: 1,
          category_id: 2,
          cloudinary_id: '["e9b48gike9oqpy0wypwv"]',
          status: 1,
          createdAt: "2022-06-24T09:44:28.186Z",
          updatedAt: "2022-06-24T09:44:28.186Z",
        },
        {
          product_name: "Kemeja Roy Marten",
          product_image:
            '["https://res.cloudinary.com/dkmcpy5fw/image/upload/v1656064065/ev7pxlom3v77egmswcmj.jpg"]',
          product_description: "Nego tipis",
          product_price: 2000000,
          user_id: 1,
          category_id: 3,
          cloudinary_id: '["ev7pxlom3v77egmswcmj"]',
          status: 1,
          createdAt: "2022-06-24T09:47:45.685Z",
          updatedAt: "2022-06-24T09:47:45.685Z",
        },
        {
          product_name: "Sweeter DC",
          product_image:
            '["https://res.cloudinary.com/dkmcpy5fw/image/upload/v1656064196/ncwp4lecvwv6fhikkt6f.jpg"]',
          product_description: "Nego tipis",
          product_price: 2500000,
          user_id: 1,
          category_id: 3,
          cloudinary_id: '["ncwp4lecvwv6fhikkt6f"]',
          status: 1,
          createdAt: "2022-06-24T09:49:57.136Z",
          updatedAt: "2022-06-24T09:49:57.136Z",
        },
        {
          product_name: "Jeans Luffy",
          product_image:
            '["https://res.cloudinary.com/dkmcpy5fw/image/upload/v1656064288/ptvh1qz1tzgpfpvpybal.jpg"]',
          product_description: "Nego tipis",
          product_price: 1500000,
          user_id: 1,
          category_id: 3,
          cloudinary_id: '["ptvh1qz1tzgpfpvpybal"]',
          status: 1,
          createdAt: "2022-06-24T09:51:29.063Z",
          updatedAt: "2022-06-24T09:51:29.063Z",
        },
        {
          product_name: "Nokia 2020",
          product_image:
            '["https://res.cloudinary.com/dkmcpy5fw/image/upload/v1656064420/kxtizv0jent9vtpicncp.jpg"]',
          product_description: "Body mulus, Nego tipis",
          product_price: 1500000,
          user_id: 1,
          category_id: 4,
          cloudinary_id: '["kxtizv0jent9vtpicncp"]',
          status: 1,
          createdAt: "2022-06-24T09:53:41.778Z",
          updatedAt: "2022-06-24T09:53:41.778Z",
        },
        {
          product_name: "Jeans Luffy",
          product_image:
            '["https://res.cloudinary.com/dkmcpy5fw/image/upload/v1656064288/ptvh1qz1tzgpfpvpybal.jpg"]',
          product_description: "Nego tipis",
          product_price: 1500000,
          user_id: 2,
          category_id: 3,
          cloudinary_id: '["ptvh1qz1tzgpfpvpybal"]',
          status: 1,
          createdAt: "2022-06-24T09:51:29.063Z",
          updatedAt: "2022-06-24T09:51:29.063Z",
        },
        {
          product_name: "Nokia 2020",
          product_image:
            '["https://res.cloudinary.com/dkmcpy5fw/image/upload/v1656064420/kxtizv0jent9vtpicncp.jpg"]',
          product_description: "Body mulus, Nego tipis",
          product_price: 1500000,
          user_id: 2,
          category_id: 4,
          cloudinary_id: '["kxtizv0jent9vtpicncp"]',
          status: 1,
          createdAt: "2022-06-24T09:53:41.778Z",
          updatedAt: "2022-06-24T09:53:41.778Z",
        },
      ],

      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
