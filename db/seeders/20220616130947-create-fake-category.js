

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "categories",
      [
        {
          category_name: "hobi",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          category_name: "kendaraan",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          category_name: "baju",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          category_name: "elektronik",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          category_name: "kesehatan",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
