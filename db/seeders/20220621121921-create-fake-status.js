

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "statuses",
      [
        {
          status_name: "wishlist",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          status_name: "accepted",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          status_name: "rejected",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          status_name: "sold",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
