

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "orders",
      [
        {
          product_id: 1,
          status_id: 1,
          user_id: 3,
          bid: 100000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          product_id: 2,
          status_id: 2,
          user_id: 1,
          bid: 200000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          product_id: 3,
          status_id: 2,
          user_id: 2,
          bid: 300000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          product_id: 4,
          status_id: 1,
          user_id: 4,
          bid: 150000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.dropTable("orders");
  },
};
